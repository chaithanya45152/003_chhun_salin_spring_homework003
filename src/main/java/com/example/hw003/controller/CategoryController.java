package com.example.hw003.controller;

import com.example.hw003.model.entity.Author;
import com.example.hw003.model.entity.Category;
import com.example.hw003.model.request.AuthorRequest;
import com.example.hw003.model.request.CategoryRequest;
import com.example.hw003.model.response.AuthorResponse;
import com.example.hw003.services.CategoryService;
import io.swagger.v3.oas.annotations.Operation;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.sql.Timestamp;
import java.util.List;
@RestController
@RequestMapping("/api/v1/categories")
public class CategoryController {
    private final CategoryService categoryService;

    public CategoryController(CategoryService categoryService) {
        this.categoryService = categoryService;
    }


    @GetMapping("/all")
    @Operation(summary = "Get all categories")
    public ResponseEntity<AuthorResponse<List<Category>>> getAllCategories(){
        AuthorResponse<List<Category>>response=AuthorResponse.<List<Category>>builder()
                .timestamp(new Timestamp(System.currentTimeMillis()))
                .status(200)
                .message("Successfully fetched categories")
                .payload(categoryService.getAllCategories())
                .build();
        return ResponseEntity.ok(response);
    }

    @GetMapping("/{categoryId}")
    @Operation(summary = "Get category by id")
    public ResponseEntity<?> getCategoryById(@PathVariable Integer categoryId) {
        AuthorResponse<Category> response = AuthorResponse.<Category>builder()
                .timestamp(new Timestamp(System.currentTimeMillis()))
                .message("Successfully fetched authors by id")
                .payload(categoryService.getCategoryById(categoryId))
                .build();
        return ResponseEntity.ok(response);

    }

    @DeleteMapping("{categoryId}")
    @Operation(summary = "Delete category by id")
    public ResponseEntity<AuthorResponse<String>> deleteCategoryIdById(@PathVariable Integer categoryId) {
        AuthorResponse<String> response = null;
        if (categoryService.deleteCategoryById(categoryId) == true) {
            response = AuthorResponse.<String>builder()
                    .timestamp(new Timestamp(System.currentTimeMillis()))
                    .status(200)
                    .message("Successfully deleted categoryId")
                    .build();
        }
        return ResponseEntity.ok(response);
    }

    @PostMapping
    @Operation(summary = "Insert new category")
    public ResponseEntity<AuthorResponse<Category>> insertCategory(@RequestBody CategoryRequest categoryRequest){
        Integer storeCategoryId = categoryService.insertCategory(categoryRequest);
        if (storeCategoryId != null){
            AuthorResponse<Category> response =AuthorResponse.<Category>builder()
                    .timestamp(new Timestamp(System.currentTimeMillis()))
                    .status(200)
                    .message("Successfully added author")
                    .payload(categoryService.getCategoryById(storeCategoryId))
                    .build();
            return ResponseEntity.ok(response);
        }
        return null;
    }

    @PutMapping("/{categoryId}")
    @Operation(summary = "Update category by id")
    public ResponseEntity<AuthorResponse<Category>> updateCategoryById(
            @RequestBody CategoryRequest categoryRequest, @PathVariable("categoryId") Integer categoryId
    ){
        AuthorResponse<Category> response = null;
        Integer idUpdate = categoryService.updateCategory(categoryRequest,categoryId);
        if (idUpdate != null){
            response = AuthorResponse.<Category>builder()
                    .timestamp(new Timestamp(System.currentTimeMillis()))
                    .status(200)
                    .message("Successfully updated category")
                    .payload(categoryService.getCategoryById(idUpdate))
                    .build();
        }
        return ResponseEntity.ok(response);
    }

}
