package com.example.hw003.controller;

import com.example.hw003.model.entity.Author;
import com.example.hw003.model.request.AuthorRequest;
import com.example.hw003.model.response.AuthorResponse;
import com.example.hw003.repository.AuthorRepository;
import com.example.hw003.services.AuthorService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.models.security.SecurityScheme;
import org.apache.ibatis.type.ArrayTypeHandler;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@RestController
@RequestMapping("/api/v1/authors")
public class AuthorController {
    private final AuthorService authorService;

    public AuthorController(AuthorService authorService) {
        this.authorService = authorService;
    }

    @GetMapping("/all")
    @Operation(summary = "Get all authors")
    public ResponseEntity<AuthorResponse<List<Author>>>  getAllAuthor(){
        AuthorResponse<List<Author>>response=AuthorResponse.<List<Author>>builder()
                .timestamp(new Timestamp(System.currentTimeMillis()))
                .status(200)
                .message("Successfully fetched authors")
                .payload(authorService.getAllAuthor())
                .build();
        return ResponseEntity.ok(response);
    }

    @GetMapping("/{authorId}")
    @Operation(summary = "Get author by id")
    public ResponseEntity<?> getAuthorById(@PathVariable Integer authorId) {
        AuthorResponse<Author> response = AuthorResponse.<Author>builder()
                .timestamp(new Timestamp(System.currentTimeMillis()))
                .message("Successfully fetched authors by id")
                .payload(authorService.getAuthorById(authorId))
                .build();
        return ResponseEntity.ok(response);

    }

    @DeleteMapping("{authorId}")
    @Operation(summary = "Delete author by id")
    public ResponseEntity<AuthorResponse<String>> deleteAuthorById(@PathVariable Integer authorId) {
        AuthorResponse<String> response = null;
        if (authorService.deleteAuthorById(authorId) == true) {
            response = AuthorResponse.<String>builder()
                    .timestamp(new Timestamp(System.currentTimeMillis()))
                    .status(200)
                    .message("Successfully deleted author")
                    .build();
        }
        return ResponseEntity.ok(response);
    }

    @PostMapping
    @Operation(summary = "Insert new author")
    public ResponseEntity<AuthorResponse<Author>> insertAuthor(@RequestBody AuthorRequest authorRequest){
        Integer storeAuthorId = authorService.insertAuthor(authorRequest);
        if (storeAuthorId != null){
            AuthorResponse<Author> response =AuthorResponse.<Author>builder()
                    .timestamp(new Timestamp(System.currentTimeMillis()))
                    .status(200)
                    .message("Successfully added author")
                    .payload(authorService.getAuthorById(storeAuthorId))
                    .build();
            return ResponseEntity.ok(response);
        }
        return null;
    }

    @PutMapping("/{authorId}")
    @Operation(summary = "Update author by id")
    public ResponseEntity<AuthorResponse<Author>> updateAuthorById(
            @RequestBody AuthorRequest authorRequest, @PathVariable("authorId") Integer authorId
    ){
        AuthorResponse<Author> response = null;
        Integer idUpdate = authorService.updateAuthor(authorRequest,authorId);
        if (idUpdate != null){
            response = AuthorResponse.<Author>builder()
                    .timestamp(new Timestamp(System.currentTimeMillis()))
                    .status(200)
                    .message("Successfully updated author")
                    .payload(authorService.getAuthorById(idUpdate))
                    .build();
        }
        return ResponseEntity.ok(response);
    }

}
