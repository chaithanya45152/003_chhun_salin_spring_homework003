package com.example.hw003.repository;

import com.example.hw003.model.entity.Author;
import com.example.hw003.model.request.AuthorRequest;
import org.apache.ibatis.annotations.*;

import java.util.List;

@Mapper
public interface AuthorRepository {
    @Select("SELECT * FROM authors")
    List<Author> findAllAuthor();

    @Select("SELECT * FROM authors WHERE id = #{authorId}")
    Author getAuthorById(Integer authorId);

    @Delete("DELETE FROM authors WHERE id = #{authorId}")
    boolean deleteAuthorById(@Param("authorId") Integer authorId);

    @Select("INSERT INTO authors (name, gender) VALUES(#{request.name}, #{request.gender}) RETURNING id")
    Integer insertAuthor(@Param("request") AuthorRequest authorRequest);

    @Select("UPDATE authors " +
            "SET name = #{request.name}," +
            "gender = #{request.gender} " +
            "WHERE id = #{authorId} " +
            "RETURNING id")
    Integer updateAuthor(@Param("request") AuthorRequest authorRequest, Integer authorId);
}
