package com.example.hw003;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class RestApi2Homework003Application {

    public static void main(String[] args) {
        SpringApplication.run(RestApi2Homework003Application.class, args);
    }

}
