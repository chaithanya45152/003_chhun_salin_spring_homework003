package com.example.hw003.exception;

public class CategoryFoundException extends RuntimeException{
    public CategoryFoundException(Integer categoryId){

        super("Book with id "+ categoryId + "not found");
    }
}
