package com.example.hw003.services;

import com.example.hw003.model.entity.Author;
import com.example.hw003.model.request.AuthorRequest;

import java.util.List;

public interface AuthorService {
    List<Author> getAllAuthor();

    Author getAuthorById(Integer authorId);

    boolean deleteAuthorById(Integer authorId);

    Integer insertAuthor(AuthorRequest authorRequest);

    Integer updateAuthor(AuthorRequest authorRequest, Integer authorId);
}



